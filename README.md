# Matlab实现经典加密算法

## 开发环境
 - version: Matlab R2017a(9.2.0.538062)
  - Matlab App Designer
 
## 内置算法
 - Caesar Cipher
 - Vigenere Cipher
 
## 为啥会有这个仓库
 上课的时候老师叫我们写个经典加密算法的函数，结果系统里不管是C、Java还是Python啥的开发环境，一个都没有，就一个Matlab，然后就有了这个仓库😂